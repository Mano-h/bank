export class Operation {
  idOperation: number;
  amountOperation: number;
  accountNumber: string;
  dateOperation: Date;
  idBeneficiary: number;
  debitAmount:number;
  creditAmount:number;
  description:string;
  balance:number
  type: string;
}
