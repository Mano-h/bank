export class Account {
  idClient: number;
  idAccount: number;
  rib: string;
  accountNumber: string;
  iban: string;
  balance: number;
  accountType: string;
  domiciliation: string;
}
