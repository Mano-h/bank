import { OperationComponent } from './operation/operation.component';
import { AccountComponent } from './account/account.component';
import { ClientComponent } from './client/client.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: '', redirectTo: '/operation', pathMatch: 'full' },
  { path: 'account', component: AccountComponent },
  { path: 'operation', component: OperationComponent },
  { path: 'client', component: ClientComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
