import { ClientService } from './../services/client.service';
import { Client } from './../class/client';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {

  public clientForm: FormGroup;
  public clients: Client[];



  constructor(private fb: FormBuilder, private service: ClientService) { }

  ngOnInit(): void {
 this.onGetAllClients;

    this.clientForm = this.fb.group({
      firstName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(10)]],
      lastName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(10)]],
      emailAdress: ['', [Validators.required, Validators.email]],
      birtDayDate: '',
      phoneNumber: '',
    });


  }


  public onsaveData() {
    if (this.clientForm?.invalid) return;
    this.service.save(this.clientForm?.value)
      .subscribe(data => {

      });
  }

  onGetAllClients(){
     this.service.getAllClient().subscribe(data =>{
       this.clients = data;
     })
  }

  onSelect(){

  }

  onEdit(Client){

  }

}
