import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Account } from '../class/account';
import { Client } from '../class/client';
import { AccountService } from '../services/account.service';
import { OperationService } from '../services/operation.service';

@Component({
  selector: 'app-operation',
  templateUrl: './operation.component.html',
  styleUrls: ['./operation.component.css']
})
export class OperationComponent implements OnInit {

  public opertationForm: FormGroup;
  public client: Client = new Client();
  public operation: any;
  public account: Account = new Account();
  displayErrorMessage: boolean = false;
  typeOperation: string = 'CREDIT';

  constructor(
    private fb: FormBuilder,
    private service: OperationService,
    private router: Router,
    private accountService: AccountService
  ) {}

  ngOnInit(): void {

    this.onGetAccountClient();

    this.opertationForm = this.fb.group({
      type: ['', [Validators.required]],
      amountOperation: [
        '',
        [
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(10),
        ],
      ],
      accountNumber: [
        '',
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(10),
        ],
      ],
      description: ['', [Validators.required, Validators.maxLength(40)]],
      beneficiary: '',
    });
  }

  public saveData() {
    if (this.opertationForm?.invalid) return;
    this.service.save(this.opertationForm?.value).subscribe(
      (data) => {

      },
      (err) => {
        console.log(' AuthentificationService KO   ' + err.message);
      }
    );
  }

  onGetAccountClient() {
    this.accountService.getAccount(this.client.id).subscribe(
      (data) => {
        this.account = data;
      },
      (err) => {
        //traitement d'erreur
      },
      () => {
        this.getAllOperationClient(this.account.accountNumber);
      }
    );
  }

  public getAllOperationClient(accountNumber) {
    this.service.getAllOperationClient(accountNumber).subscribe(
      (data) => {
        this.operation = data;
      },
      (err) => {
        console.log(err);
      }
    );
  }


}
