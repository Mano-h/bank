package com.kata.bank.business.operation;

import com.kata.bank.entity.Account;
import com.kata.bank.entity.Operation;
import com.kata.bank.execption.FunctionnalException;
import com.kata.bank.mapper.Mapper;
import com.kata.bank.model.OperationModel;
import com.kata.bank.repository.account.AccountRepository;
import com.kata.bank.repository.client.ClientRepository;
import com.kata.bank.repository.operation.OperationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OperationsBusinesImpl implements OperationsBusiness {
  private static final String DEBITOPERATION = "DEBIT";
  @Autowired private AccountRepository accountRepository;
  @Autowired private ClientRepository clientRepository;
  @Autowired private OperationRepository operationRepository;

  @Override
  public Operation transaction(OperationModel operationModel) {

    Account currentAccount = findAccount(operationModel);

    if (balanceCheck(currentAccount.getBalance(), operationModel.getAmountOperation()) == -1
        && operationModel.getType().equals(DEBITOPERATION)) {
      throw new FunctionnalException("SOLDE_INSUFFISANT", "400");
    }

    Operation operation = Mapper.mapFromModel(operationModel, currentAccount);
    currentAccount.setBalance(operation.getBalance());
    operation.setAccount(currentAccount);
    operation.setBalance(currentAccount.getBalance());

    return operationRepository.save(operation);
  }

  @Override
  public Operation withdrawal(OperationModel operationModel) {
    return null;
  }

  private int balanceCheck(Double accountBalance, Double amountOperation) {
    return accountBalance.compareTo(amountOperation);
  }

  @Override
  public List<Operation> findAllOperationByClient(Long idClient) {
    return null;
  }

  @Override
  public List<Operation> findAllOperations() {
    return operationRepository.findAll();
  }

  @Override
  public List<Operation> findOperationByAccount(String accountNumber) {
    Account account =
        accountRepository
            .findAccountByAccountNumber(accountNumber)
            .orElseThrow(() -> new FunctionnalException("MISSING_DEPOSIT_ACCOUNT", "400"));
    return operationRepository.findOperationByAccount(account);
  }

  private Account findAccount(OperationModel operationModel) {
    return accountRepository
        .findAccountByAccountNumber(operationModel.getAccountNumber())
        .orElseThrow(() -> new FunctionnalException("MISSING_DEPOSIT_ACCOUNT", "400"));
  }

  private Account findBeneficiary(OperationModel operationModel) {
    return accountRepository
        .findAccountByAccountNumber(operationModel.getAccountNumber())
        .orElseThrow(() -> new FunctionnalException("MISSING_DEPOSIT_ACCOUNT", "400"));
  }
}
