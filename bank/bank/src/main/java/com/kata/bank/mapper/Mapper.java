package com.kata.bank.mapper;

import com.kata.bank.entity.Account;
import com.kata.bank.entity.Client;
import com.kata.bank.entity.Operation;
import com.kata.bank.model.AccountModel;
import com.kata.bank.model.ClientModel;
import com.kata.bank.model.OperationModel;

import java.time.LocalDate;
import java.util.Random;

public class Mapper {

  private static final String DEBITOPERATION = "DEBIT";
  private static final String CREDITOPERATION = "CREDIT";

  public static Client mapFromModel(ClientModel clientModel) {
    Client client = new Client();
    client.setLastName(clientModel.getLastName());
    client.setFirstName(clientModel.getFirstName());
    client.setBirtDayDate(clientModel.getBirtDayDate());
    client.setAccountNumber(String.valueOf(new Random().nextInt(987654321)));
    client.setClientId(String.valueOf(new Random().nextInt(987654321)));

    if (clientModel.getId() != null) {
      client.setId(clientModel.getId());
    }
    return client;
  }

  public static Account mapFromModel(AccountModel accountModel) {
    Account account = new Account();
    account.setAccountNumber(accountModel.getAccountNumber());
    account.setAccountType("EPARGNE");
    account.setBalance(accountModel.getBalance());
    account.setDomiciliation(accountModel.getDomiciliation());
    account.setIban("FR7612548029" + accountModel.getAccountNumber() + accountModel.getIdClient());
    account.setRib(
        "FR76300040000" + accountModel.getAccountNumber() + "4" + accountModel.getIdClient());
    return account;
  }

  public static Operation mapFromModel(OperationModel operationModel, Account account) {
    Operation operation = new Operation();
    operation.setDateOperation(LocalDate.now());
    operation.setDescription(operationModel.getDescription());
    if (operationModel.getType().equals(DEBITOPERATION)) {
      operation.setDebitAmount(operationModel.getAmountOperation());
      operation.setCreditAmount(Double.valueOf(0.0));
      operation.setBalance(Double.sum(account.getBalance(), -operationModel.getAmountOperation()));
    } else if (operationModel.getType().equals(CREDITOPERATION)) {
      operation.setCreditAmount(operationModel.getAmountOperation());
      operation.setDebitAmount(Double.valueOf(0.0));
      operation.setBalance(Double.sum(account.getBalance(), operationModel.getAmountOperation()));
    }
    return operation;
  }
}
