package com.kata.bank.repository.account;

import com.kata.bank.entity.Account;
import com.kata.bank.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account,Long> {
    Account findAccountByClients(Client client);
    Optional<Account> findAccountByAccountNumber(String accountNumber);
}
