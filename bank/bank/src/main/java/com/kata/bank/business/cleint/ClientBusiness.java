package com.kata.bank.business.cleint;

import com.kata.bank.entity.Client;
import com.kata.bank.model.ClientModel;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface ClientBusiness {
  Client save(ClientModel clientModel);

  Client updateClient(ClientModel clientModel);

  Optional<Client> findClientByid(Long idClient);

  List<Client> findClientAll();

  Client findClientByAccountNumber(String accountNumber);
}
