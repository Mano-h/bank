package com.kata.bank.business.account;

import com.kata.bank.entity.Account;
import com.kata.bank.model.AccountModel;

import java.util.List;

public interface AccountBusiness {
  Account save(AccountModel accountModel);

  Account updateClient(AccountModel accountModel);

  Account findAccountByNumber(String accountNumber);

  Account findAccountByIdClient(Long id);

  List<Account> findAllAccount();
}
