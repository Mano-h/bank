package com.kata.bank.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ErrorRessource {
    private String code;
    private String message;
}
