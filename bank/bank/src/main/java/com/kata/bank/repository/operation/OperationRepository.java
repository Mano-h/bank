package com.kata.bank.repository.operation;

import com.kata.bank.entity.Account;
import com.kata.bank.entity.Operation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OperationRepository extends JpaRepository<Operation, Long> {
    List<Operation> findOperationByAccount(Account account);
}
