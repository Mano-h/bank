package com.kata.bank.business.cleint;

import com.kata.bank.entity.Client;
import com.kata.bank.execption.FunctionnalException;
import com.kata.bank.mapper.Mapper;
import com.kata.bank.model.ClientModel;
import com.kata.bank.repository.client.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClientBusinessImp implements ClientBusiness {
  @Autowired private ClientRepository clientRepository;

  @Override
  public Client save(ClientModel clientModel) {
    Client client = Mapper.mapFromModel(clientModel);
    client.setPassword(client.getClientId());
    return clientRepository.save(client);
  }

  @Override
  public Client updateClient(ClientModel clientModel) {
    return clientRepository.save(Mapper.mapFromModel(clientModel));
  }

  @Override
  public Optional<Client> findClientByid(Long idClient) {
    return clientRepository.findById(idClient);
  }

  @Override
  public List<Client> findClientAll() {
    return clientRepository.findAll();
  }

  @Override
  public Client findClientByAccountNumber(String accountNumber) {
    return clientRepository
        .findClientByAccountNumber(accountNumber)
        .orElseThrow(() -> new FunctionnalException("MISSING_CLIENT_FOR_THIS_ACCOUNT", "400"));
  }
}
