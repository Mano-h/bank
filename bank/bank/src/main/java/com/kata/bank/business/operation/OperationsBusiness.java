package com.kata.bank.business.operation;

import com.kata.bank.entity.Operation;
import com.kata.bank.model.OperationModel;

import java.util.List;

public interface OperationsBusiness {
  Operation transaction(OperationModel operationModel);

  Operation withdrawal(OperationModel operationModel);

  List<Operation> findAllOperationByClient(Long idClient);

  List<Operation> findAllOperations();

  List<Operation> findOperationByAccount(String accountNumber);
}
