package com.kata.bank.data;

import com.kata.bank.business.operation.OperationsBusiness;
import com.kata.bank.entity.Account;
import com.kata.bank.entity.Client;
import com.kata.bank.model.OperationModel;
import com.kata.bank.repository.account.AccountRepository;
import com.kata.bank.repository.client.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDate;


@Component
public class InitData {

    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private OperationsBusiness operationsBusiness;


    @PostConstruct
    private void initdata() {

        Client hermann = new Client();
        hermann.setBirtDayDate(LocalDate.of(1986, 05, 20));
        hermann.setFirstName("Emilie");
        hermann.setLastName("Pages");
        hermann.setPhoneNumber("+3305342536");
        hermann.setClientId("12345678");
        hermann.setPassword(hermann.getClientId());
        hermann.setAccountNumber("9876543210");

        clientRepository.save(hermann);
        //User Test

        Client clientTest = new Client();
        clientTest.setBirtDayDate(LocalDate.of(1986, 05, 20));
        clientTest.setFirstName("Raoul");
        clientTest.setLastName("MiNO");
        clientTest.setPhoneNumber("+3305342537");
        clientTest.setClientId("898989889");
        clientTest.setPassword(hermann.getClientId());
        clientTest.setAccountNumber("8888888888");

        clientRepository.save(clientTest);

        // Account

        Account account = new Account();

        account.setClients(hermann);
        account.setIban("FR7612548029989876543210917.");
        account.setAccountNumber(hermann.getAccountNumber());
        account.setAccountType("EPARGNE");
        account.setBalance(Double.valueOf(0.0));
        account.setDomiciliation("SG MELUN");
        account.setRib("FR7630004000031234567890143 43");
        accountRepository.save(account);

        //Operation
        OperationModel operationCredit = new OperationModel();

        operationCredit.setAccountNumber(account.getAccountNumber());
        operationCredit.setAmountOperation(Double.valueOf(3500));
        operationCredit.setDateOperation(LocalDate.now());
        operationCredit.setDescription("virement salaire");
        operationCredit.setType("CREDIT");

        operationsBusiness.transaction(operationCredit);

       /* OperationModel operationDebit = new OperationModel();

        operationDebit.setAccountNumber(account.getAccountNumber());
        operationDebit.setAmountOperation(Double.valueOf(200));
        operationDebit.setDateOperation(LocalDate.now());
        operationDebit.setDescription("Paiement EDF");
        operationDebit.setType("DEBIT");

        operationsBusiness.transaction(operationDebit);*/





    }
}
