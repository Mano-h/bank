package com.kata.bank.controller.account;

import com.kata.bank.business.account.AccountBusiness;
import com.kata.bank.entity.Account;
import com.kata.bank.model.AccountModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("account")
public class AccountController {
    @Autowired
    private AccountBusiness accountBusiness;

    @PostMapping
    public ResponseEntity<Account> save(@RequestBody AccountModel accountModel) {
        return new ResponseEntity<Account>(accountBusiness.save(accountModel), HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<Account>> findAccounts() {
        return new ResponseEntity<List<Account>>(accountBusiness.findAllAccount(), HttpStatus.ACCEPTED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Account> findAccountByIdClient(@PathVariable Long id) {
        return new ResponseEntity<Account>(
                accountBusiness.findAccountByIdClient(id), HttpStatus.ACCEPTED);
    }

}
