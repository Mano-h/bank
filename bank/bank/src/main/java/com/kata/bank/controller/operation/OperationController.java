package com.kata.bank.controller.operation;

import com.kata.bank.business.operation.OperationsBusiness;
import com.kata.bank.entity.Operation;
import com.kata.bank.model.OperationModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("operation")
public class OperationController {


    @Autowired
    private OperationsBusiness operationsBusiness;

    @PostMapping
    public ResponseEntity<Operation> deposit(@RequestBody OperationModel operationModel) {
        return new ResponseEntity<Operation>(
                operationsBusiness.transaction(operationModel), HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<Operation>> findAllOperationClientById() {
        return new ResponseEntity<List<Operation>>(
                operationsBusiness.findAllOperations(), HttpStatus.ACCEPTED);
    }

    @GetMapping("/{accountNumber}")
    public ResponseEntity<List<Operation>> findAllOperationByAccountNumber(
            @PathVariable String accountNumber) {
        return new ResponseEntity<List<Operation>>(
                operationsBusiness.findOperationByAccount(accountNumber), HttpStatus.ACCEPTED);
    }
}
