package com.kata.bank.controller.client;

import com.kata.bank.business.cleint.ClientBusiness;
import com.kata.bank.entity.Client;
import com.kata.bank.model.ClientModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("client")
public class ClientController {

  @Autowired private ClientBusiness clientBusiness;

  @PostMapping
  public ResponseEntity<Client> saveClient(@RequestBody ClientModel clientModel) {
    return ResponseEntity.ok(clientBusiness.save(clientModel));
  }

  @PatchMapping
  public ResponseEntity<Client> updateClient(@RequestBody ClientModel clientModel) {
    return ResponseEntity.ok(clientBusiness.updateClient(clientModel));
  }

  @GetMapping("/{id}")
  public ResponseEntity<Client> findClientById(@PathVariable Long id) {
    return ResponseEntity.ok(clientBusiness.findClientByid(id).get());
  }

  @GetMapping("/all")
  public ResponseEntity<List<Client>> findAllClient() {
    return ResponseEntity.ok(clientBusiness.findClientAll());
  }
}
